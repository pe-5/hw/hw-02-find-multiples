// ------------------------ Task 2 ----------------------------
console.log('Task 2 ---------->');

let m = prompt('Task 2. \nEnter first number:');

while (m === null || m < 2 || Number.isNaN(+m) || m.trim() === '' || !Number.isInteger(+m)){
  alert('Error!!! Enter correct data!!!');
  m = prompt('Task 2. \nEnter correct first number:', m);
}

let n = prompt('Task 2. \nEnter second number:');

while (n === null || n <= m || Number.isNaN(+n) || n.trim() === '' || !Number.isInteger(+n)){
  alert('Error!!! Enter correct data!!!');
  n = prompt('Task 2. \nEnter correct second number:', n);
}

n = +n;
m = +m;

function isPrime(num){
    for(let i = 2; i < num; i ++){
      if (num % i === 0)
        return false;
    }
    return num ;
  }

for (let i = m; i < n; i++) {
    let prime = isPrime(i);
  
    if (prime)
      console.log(prime);
}
