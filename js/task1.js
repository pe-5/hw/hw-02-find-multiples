// ------------------------ Task 1 ----------------------------
console.log('Task 1 ---------->');

let userNum;
let noNum = true;

do{
  userNum = prompt('Task 1. \nEnter number:', userNum);
}
while(userNum === null || Number.isNaN(+userNum) || userNum.trim() === '' || !Number.isInteger(+userNum));

if(userNum >=0){
  for(let i = 1; i <= userNum; i++){
    if (i % 5 === 0){
      console.log(i);
      noNum = false;
    }
  }
}
else{
  for(let i = -1; i >= userNum; i--){
    if (i % 5 === 0){
      console.log(i);
      noNum = false;
    }
  }
}

if (noNum)
  console.log('Sorry, no numbers');

